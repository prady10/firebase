const functions = require('firebase-functions');
const esqbuilder = require('elastic-builder');
const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
    host: 'http://user:yuCgj1JjSDjJ@35.226.58.202/elasticsearch'
  });
// full text search handler for elastic search
exports.elasticSearchQuery = functions.https.onRequest((request, response) => {

    // get the query from the params
    var clientQuery = request.query.query;
    var queryType = request.query.queryType;

    var query = esqbuilder.requestBodySearch().query(esqbuilder.
      multiMatchQuery(["brand", "name", "description", "serving_size", "categories"], clientQuery)
      .fuzziness("AUTO"));
    // // build the elastic search DSL query based on the query type param
    switch (queryType) {
      case "all": 
      query = esqbuilder.requestBodySearch().query(esqbuilder.
      multiMatchQuery(["brand", "name", "description", "serving_size", "categories"], clientQuery)
      .fuzziness("AUTO"));
      break;
      case "brand":
      query = esqbuilder.requestBodySearch().query(esqbuilder.
        multiMatchQuery(["brand", "description"], clientQuery)
        .fuzziness("AUTO"));
        break;
      case "name":
      query = esqbuilder.requestBodySearch().query(esqbuilder.
        multiMatchQuery(["name", "description"], clientQuery)
        .fuzziness("AUTO"));
        break;
      case "ingredient":
      query = esqbuilder.requestBodySearch().query(esqbuilder.
        multiMatchQuery(["ingredientNames", "ingredientDescriptions"], clientQuery)
        .fuzziness("AUTO"));
        break;
      default:
      query = esqbuilder.requestBodySearch().query(esqbuilder.
        multiMatchQuery(["brand", "name", "description", "serving_size", "categories"], clientQuery)
        .fuzziness("AUTO"));
        break;
    }
    console.log('client query', clientQuery);
    client.search({
        body: query,
        size: 10000
      })
      .then(resp => {
        console.log(resp);
        const hits = {
          "hits" :resp.hits.hits
        };
        return response.status(200).send(hits)
      })
      .catch(err => {
        throw new Error(err.message);
      });
});
// category search handler for one or more categories
exports.elasticSearchByCategory = functions.https.onRequest((request, response) => {
      // grab the categories to filter
      var categoriesToFilter = request.body.categories;
      var mustQueries = [];
      // loop through categories creating must match query item
      categoriesToFilter.forEach(category => {
        mustQueries.push(esqbuilder.matchQuery("categories", category))
      });
      if (mustQueries.length > 0) {
        // create the query 
        var elasticQuery = esqbuilder.requestBodySearch().query(
          esqbuilder.boolQuery()
          .must(mustQueries));
          client.search({
            body: elasticQuery,
            size: 10000
          })
          .then(resp => {
            console.log(resp);
            const hits = {
              "hits" :resp.hits.hits
            };
            return response.status(200).send(hits)
          })
          .catch(err => {
            throw new Error(err.message);
          });
      } else {
        response.status(200).send({ "count": 0, "hits": 0 });
      }
});

/// MARK : AmazonProductAdvertising API Middleware 
const amazon = require('amazon-product-api');
const client = amazon.createClient({
    awsId: "AKIAJEQR7N4Q2L4TITAQ",
    awsSecret: "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",
    awsTag: "jhall03200a-20"
});

exports.getAmazonProductInfo = functions.https.onRequest((request, response) => {

    // grab the scan type and code from query params
    var scan_type = request.query.scan_type;
    var code = request.query.code;

    client.itemLookup({
        idType: scan_type,
        itemId: code,
        searchIndex: 'HealthPersonalCare',
        responseGroup: ['ItemAttributes', 'Reviews', 'SalesRank', 'EditorialReview']
      }).then(function(results) {
        var resp = { 
            "results" : results
        }
        response.status(200).send(resp);
      }).catch(function(err) {
        console.log(err);
        response.status(404).send(err);
      });
});
