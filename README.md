# Elastic Search Supplement Snoop API


 ## Full Text Search
 
`https://us-central1-supplement-snoop.cloudfunctions.net/elasticSearchQuery?`


### Required Parameters

- query(string)

### Example Query

 `query=protein`

 
 ## Filter Categories
 
`https://us-central1-supplement-snoop.cloudfunctions.net/elasticSearchByCategory`


## Required Post Body Properties

- categories[string]

## Example Body

```
{
    categories: ["Increase Energy", "Protein", "Supplement Snoop Verified"]
}
```

## Amazon Product Advertising API Middleware
 
`https://us-central1-supplement-snoop.cloudfunctions.net/getAmazonProductInfo`


## Required Query Parameters

- scan_type[string]
- code[string]

### Example Query

 `scan_type=EAN&code=184732833`